<?php

namespace Tests\Unit;

use App\Car;
use App\User;
use Tests\TestCase;

class CarTest extends TestCase
{
    public function testCarCreatedSuccessfully()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user, 'api');

        $carData = [
            'make' => 'Ford',
            'model' => 'F150',
            'year' => 2016,
        ];

        $this->json('POST', 'api/add-car', $carData, ['Accept' => 'application/json'])
            ->assertStatus(201)
            ->assertJson([
                'data' => [
                    'make' => 'Ford',
                    'model' => 'F150',
                    'year' => 2016,
                ],
            ]);
    }

    public function testCarListedSuccessfully()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user, 'api');

        factory(Car::class)->createMany([
            [
                'make' => 'Ford',
                'model' => 'F150',
                'year' => 2016,
            ],
            [
                'make' => 'Audi',
                'model' => 'A3',
                'year' => 2015,
            ]
        ]);

        $this->json('GET', 'api/get-cars', ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                "data" => [
                    [
                        'make' => 'Ford',
                        'model' => 'F150',
                        'year' => 2016,
                    ],
                    [
                        'make' => 'Audi',
                        'model' => 'A3',
                        'year' => 2015,
                    ]
                ]
            ]);
    }
}
