<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Car extends Model
{
    /**
     * @var string[]
     */
    protected $fillable = ['make', 'model', 'year'];

    /**
     * @var string[]
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * Return trips for car.
     *
     * @return HasMany
     */
    public function trips(): HasMany
    {
        return $this->hasMany(Trip::class);
    }

    public function getMilesAttribute()
    {
        $miles = 0;

        foreach($this->trips as $trip){
            $miles += $trip->miles;
        }

        return $miles;
    }
}
