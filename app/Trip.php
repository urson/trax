<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Trip extends Model
{
    /**
     * @var string[]
     */
    protected $fillable = ['date', 'car_id', 'miles'];

    /**
     * @var string[]
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'date',
    ];

    /**
     * Trip belongs to one car.
     *
     * @return BelongsTo
     */
    public function car(): BelongsTo
    {
        return $this->belongsTo(Car::class);
    }
}
