<?php
declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Http\Requests\TripStoreRequest;
use App\Http\Resources\TripResource;
use App\Trip;
use Illuminate\Http\JsonResponse;

class TripController extends Controller
{
    /**
     * Return all trips.
     * (This request don't support pagination, because of "total").
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $data = [];

        // Get all trips with cars.
        $trips = Trip::with('car')->orderBy('date', 'asc')->get();

        $total = 0;
        foreach ($trips as $trip) {
            $total += $trip->miles;

            $data[] = [
                'id' => $trip->id,
                'date' => $trip->date->format('m/d/Y'),
                'miles' => $trip->miles,
                'total' => $total,
                'car' => [
                    'id' => $trip->car->id,
                    'make' => $trip->car->make,
                    'model' => $trip->car->model,
                    'year' => $trip->car->year
                ]
            ];
        }

        return response()->json([
            'data' => array_reverse($data),
        ], 200);
    }

    /**
     * Store trip.
     *
     * @param TripStoreRequest $request
     * @return TripResource
     */
    public function store(TripStoreRequest $request): TripResource
    {
        // Validate data.
        $validated = $request->validated();

        // Add new trip.
        $trip = Trip::create([
            'date' => $validated['date'],
            'car_id' => $validated['car_id'],
            'miles' => $validated['miles'],
        ]);

        return new TripResource($trip);
    }
}
