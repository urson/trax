<?php
declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Car;
use App\Http\Requests\CarStoreRequest;
use App\Http\Resources\CarResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class CarController extends Controller
{
    /**
     * Return all cars.
     *
     * @return AnonymousResourceCollection
     */
    public function index(): AnonymousResourceCollection
    {
        return CarResource::collection(Car::all());
    }

    /**
     * Store car.
     *
     * @param CarStoreRequest $request
     * @return CarResource
     */
    public function store(CarStoreRequest $request): CarResource
    {
        $validated = $request->validated();

        $car = Car::create([
           'make' => strip_tags($validated['make']),
           'model' => strip_tags($validated['model']),
           'year' => $validated['year'],
        ]);

        return new CarResource($car);
    }

    /**
     * Return car data.
     *
     * @param Car $car
     * @return CarResource
     */
    public function show(Car $car): CarResource
    {
        return new CarResource($car);
    }

    /**
     * Delete car.
     *
     * @param Car $car
     * @return JsonResponse
     * @throws \Exception
     */
    public function destroy(Car $car): JsonResponse
    {
        $car->delete();

        return response()->json(null, 204);
    }
}
