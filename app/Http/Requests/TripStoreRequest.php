<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TripStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'date' => 'required|date',
            'car_id' => 'required|integer|exists:cars,id',
            'miles' => 'required|numeric',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'date.required' => 'Date is required.',
            'car_id.required' => 'Car is required.',
            'car_id.exists' => 'This car don\'t exist.',
            'miles.required' => 'Miles is required.',
            'miles.numeric' => 'Must be a number.',
        ];
    }
}
