<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CarSeeder extends Seeder
{
    /**
     * Create some cars.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cars')->insert([
            'make' => 'Land Rover',
            'model' => 'Range Rover Sport',
            'year' => '2017',
            'created_at' => Carbon::now()
        ]);
        DB::table('cars')->insert([
            'make' => 'Ford',
            'model' => 'F150',
            'year' => '2014',
            'created_at' => Carbon::now()
        ]);
        DB::table('cars')->insert([
            'make' => 'Chevy',
            'model' => 'Tahoe',
            'year' => '2015',
            'created_at' => Carbon::now()
        ]);
        DB::table('cars')->insert([
            'make' => 'Aston Martin',
            'model' => 'Vanquish',
            'year' => '2018',
            'created_at' => Carbon::now()
        ]);
    }
}
