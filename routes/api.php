<?php

use Illuminate\Http\Request;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

/*
 * Cars
 */
// Endpoint to get all cars for the logged in user
Route::get('/get-cars', 'Api\CarController@index')->middleware('auth:api');
// Endpoint to get a car with the given id
Route::get('/get-car/{car}', 'Api\CarController@show')->middleware('auth:api');
// Endpoint to store new car.
Route::post('/add-car', 'Api\CarController@store')->middleware('auth:api');
// Endpoint to delete a car with a given id
Route::delete('/delete-car/{car}', 'Api\CarController@destroy')->middleware('auth:api');

/*
 * Trips
 */
// Endpoint to get the trips for the logged in user
Route::get('/get-trips', 'Api\TripController@index')->middleware('auth:api');
// Endpoint to add a new trip.
Route::post('/add-trip', 'Api\TripController@store')->middleware('auth:api');
